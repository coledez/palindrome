#include <string>
#include <iostream>

// Calcul d'un palindrome centré sur les positions left et right
// (Soit de taille paire, si left et right sont côte à côte)
// (Soit de taille impaire si left et right sont séparés d'un)
std::string un_palindrome(const std::string& chaine, int left, int right)
{
    int taille = chaine.size();

    while (chaine[left] == chaine[right] && left > 0 && right < taille)
    {
        left --;
        right ++;
    }

    // Si on a fait une itération de trop, on revient en arrière
    if (chaine[left] != chaine[right])
    {
        left ++;
        right --;
    }

    return chaine.substr(left, right - left + 1);
}

// Calcul du plus long palindrome d'une chaine
std::string plus_long_palindrome(const std::string& chaine)
{
    std::string max = "";
    std::string palindrome;
    int taille = chaine.size();

    if (taille > 0)
    {
        max += chaine[0];
    }

    int i = 0;

    // On parcourt toute la chaîne
    // Mais on s'arrête si ce qu'il reste à parcourir est plus court que la moitié
    // du plus long palindrome trouvé (car dans ce cas on ne pourra au mieux que 
    // l'égaler)
    while (i < taille && taille - i > max.size()/2)
    {
        //std::cout << "i : " << i << std::endl;

        // Palindromes de taille paire
        int left = i;
        int right = i+1;

        palindrome = un_palindrome(chaine, left, right);

        //std::cout << palindrome << std::endl;

        if (palindrome.size() > max.size())
        { 
            max = palindrome;
        }

        // Palindromes de taille impaire (avec une lettre unique au milieu)
        if (i < taille - 2)
        {
            left = i;
            right = i + 2;

            palindrome = un_palindrome(chaine, left, right);

            //std::cout << palindrome << std::endl;

            if (palindrome.size() > max.size())
            { 
                max = palindrome;
            }
        }
        i++;
    }

    return max;
}

int main()
{
    std::string chaine = "aaabacabababa";

    std::cout << "chaine : " << chaine << std::endl;

    std::string palindrome = plus_long_palindrome(chaine);

    std::cout << "plus long palindrome : " << palindrome << std::endl;

    return 0;
}